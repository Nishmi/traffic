#ifndef C_SIGNALS_H
#define C_SIGNALS_H

#include<iostream>
#include<vector>

class C_Signals
{
    private : std::string sig_name;
              std::string rd_name;
              int density;
              double avg_switch;
              std::vector<C_Signals *>signal;

    public :
             void init(std::string name ,std::string rd);
             void add_connections(C_Signals *next);
             int get_density();
             void set_density(int val);
             double get_switch();
             void set_switch(double time);
             C_Signals* get_connections(std::string s);
             std::string getName();
             void display();
};
#endif // C_SIGNALS_H

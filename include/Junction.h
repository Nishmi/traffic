#ifndef JUNCTION_H
#define JUNCTION_H

#include <vector>
#include <GL/freeglut.h>
#include "Road.h"

class Junction
{
    float x, y;
    bool oneway = false;
    bool signalState = true;
    bool isSignal = true;
    std::vector<Junction*> junctions;
    public:
        Junction();
        Junction(float, float);
        bool isOneWay();
        void add(Junction*);
        void draw();
        float getX();
        float getY();
        virtual ~Junction();
    protected:
    private:
};

#endif // JUNCTION_H

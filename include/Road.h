#ifndef ROAD_H
#define ROAD_H

#include <GL/freeglut.h>

class Road
{
    float startx, starty, endx, endy;
    public:
        Road();
        Road(float, float , float, float);
        void draw(float, float, float, float, float, float);
        virtual ~Road();
    protected:
    private:
};

#endif // ROAD_H

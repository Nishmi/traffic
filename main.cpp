#include <iostream>
#include <GL/freeglut.h>
#include "Junction.h"
#include "C_Signals.h"

void render();

//Junction
//A, B, C, D;
struct Signal
{
    int time[5];
    int den[5];
};

Road
A(0.5, 0.5, 0,0),
B(0,0,-0.5, 0.5),
C(-0.5,0.5, -0.5,-0.5);
//D();
unsigned int count = 0;

int main(int argn, char** argc)
{
    glutInit(&argn, argc);
    glutInitWindowSize(700, 700);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutCreateWindow("Traffic Simulation");


    //A = Junction(0,0);
    //B = Junction(0.5, 0.5);
    //C = Junction(-0.5, 0.5);
    //D = Junction(-0.5, -0.5);

    //A.add(&B);
    //A.add(&C);
    //A.add(&D);
    //C.add(&D);

    C_Signals sig_A,sig_B,sig_C,sig_D;
    struct Signal A;
    sig_A.init("Signal A","Road A");
    sig_B.init("Signal B","Road B");
    sig_C.init("Signal C","Road C");
    sig_D.init("Signal D","Road D");

    int tim_coun=1,i;
    int densi = 50;
    for(i=0; i<5; i++){
        A.time[i] = tim_coun;
        tim_coun++;
        A.den[i] = densi;
        densi += 10;
    }
    std::cout << "Traffic Density Max at "<<A.time[4] << std::endl;


    glutDisplayFunc(render);
    glutIdleFunc(render);
    glutMainLoop();

    return 0;
}

void render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glLineWidth(10);

    A.draw(1,1,1,1,0,0);
    B.draw(1,1,1,1,1,1);
    C.draw(1,1,1,1,1,1);



    glLineWidth(0.1);
    glBegin(GL_LINE_STRIP);
    glColor3f(1,1,1);
    glVertex2f(0,0);
    glVertex2f(0, -0.5);
    glVertex2f(-0.5,-0.5);
    glEnd();

    glLineWidth(0.1);
    glBegin(GL_LINE_STRIP);
    glVertex2f(0.5,0.5);
    glVertex2f(0.5, 0);
    glVertex2f(0.5,-0.5);
    glVertex2f(0, -0.5);
    glEnd();

    if(count > 300){
        glLineWidth(10);
        A.draw(1,1,1,1,1,1);
    }
    glBegin(GL_LINE_STRIP);
    glColor3f(1,1,1);
    glVertex2f(0,0);
    glVertex2f(0, 0.5);
    glVertex2f(-0.5, 0.5);
    glEnd();

    //glline

    glutSwapBuffers();
    count++;
}

#include"C_Signals.h"

void C_Signals :: add_connections(C_Signals *next)
{
    signal.push_back(next);
}
int C_Signals::get_density()
{
    return this->density;
}
void C_Signals ::set_density(int val)
{
    this->density = val;
}
double C_Signals::get_switch()
{
    return this->avg_switch;
}
void C_Signals::set_switch(double time)
{
    this->avg_switch = time;
}
C_Signals* C_Signals::get_connections( std::string s)
{
    unsigned int i;
    C_Signals *str;
    for(i=0;i<signal.size();i++){
        if(signal.at(i)->getName() == s) {
            str = signal.at(i);
        }
    }
    return str;
}
std::string C_Signals::getName()
{
    return this->rd_name;
}
void C_Signals:: init(std::string name,std::string rd)
{
    this->sig_name = name;
    this->rd_name = rd;
}

void C_Signals :: display()
{
    std :: cout << sig_name << " ";
    std :: cout << rd_name << "\n";
}

#include "Junction.h"

Junction::Junction()
{
    //ctor
}

Junction::Junction(float a, float b)
{
    x = a;
    y = b;
}

float Junction::getX() { return x; }
float Junction::getY() { return y; }

void Junction::add(Junction *j)
{
    junctions.push_back(j);
}

void Junction::draw()
{
    int i;
    for(i=0;i<junctions.size();i++)
    {
        Junction *j = junctions.at(i);
        if(!j->isOneWay())
        {
            float m = (j->getY() - y) / (j->getX() - x);
            float mo = -1/m;
            float
            x1 = x + 0.01,
            x2 = x - 0.01,
            y1 = m*x1,
            y2 = m*x2,
            xj1 = j->getX() + 0.01,
            xj2 = j->getX() - 0.01,
            yj1 = m*xj1,
            yj2 = m*xj2;
            Road(x1, y1, xj1, xj2).draw(1, 1, 1);
            Road(x2, y2, xj1, xj2).draw(1, 1, 1);
        }
        Road(x, y, j->getX(), j->getY()).draw(1, 1, 1, 1, 0, 0);
        if(isSignal)
        {
            glPointSize(10);
            glBegin(GL_POINTS);
            if(signalState)
            {
                glColor3f(0, 0.8, 0);
            }
            else
            {
                glColor3f(0.8, 0, 0);
            }
            glVertex2f(j->getX(), j->getY());
            glEnd();
        }
    }

}

bool Junction::isOneWay() { return oneway; }

Junction::~Junction()
{
    //dtor
}

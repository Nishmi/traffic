#include "Road.h"

Road::Road()
{
    //ctor
}

Road::Road(float sx, float sy, float ex, float ey): startx(sx), starty(sy), endx(ex), endy(ey)
{

}

void Road::draw(float red, float green, float blue, float r, float g, float b)
{
    glBegin(GL_LINES);
    glColor3f(red, green, blue);
    glVertex2f(startx, starty);
    glColor3f(r,g,b);
    glVertex2f(endx, endy);
    glEnd();
}

Road::~Road()
{
    //dtor
}
